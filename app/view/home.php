<div id="section1">
	<div class="row">
		<div class="sec1Left col fl">
			<img src="public/images/content/sec1img.jpg" alt="Engine">
		</div>
		<div class="sec1Right col fl">
			<h1>Chang Auto Repair</h1>
			<p>With over 25 years of experience, Chang Auto Repair at Sterling, Virginia has been helping customers in Washington, DC Metro area with their automotive needs and has become the go to auto repair.</p>
			<p>The experienced team at Chang  Auto Repair takes pride in going the extra mile for our customers. Since we know that getting a repair can be frustrating, we try to work around your schedule and service your car quickly and correctly the first time. Whether you need an oil change, shocks, spark plugs, alignment or a complete engine exchange – we do it all!</p>
			<a href="about#content" class="btn">LEARN MORE</a>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="section2">
	<div class="row">
		<div class="sec2Left col fl">
			<h3>WELCOME</h3>
			<p>Thanks for visiting Chang Auto Repair at Sterling,VA. With over 25 years of work experience with Major dealerships, we provide Collision Repairs including Major Body Repair, Small Dents and Dings, Frame Pulls, Full Service Painting and Restoration of old cars. Our team of Expert Technicians use the state of the art High-Tech Equipment and deliver quality workmanship.</p>
			<a href="contact#content" class="btn">FREE ESTIMATE</a>
		</div>
		<div class="sec2Right col fl">
			<img src="public/images/content/sec2img.jpg" alt="Wheels">
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="section3">
	<div class="row">
		<h2>Our Services</h2>
		<div class="sec3Top">
			<dl class="col fl">
				<dt> <img src="public/images/content/sec3img1.jpg" alt="Service Image 1"> </dt>
				<dd>
					<ul>
						<li> <span>•</span> Oil Change</li>
						<li> <span>•</span> Pre-Purchase Vehicle Inspection</li>
						<li> <span>•</span> Scheduled Car Maintenance</li>
						<li> <span>•</span> New Tires & Tire Related Services</li>
						<li> <span>•</span> Flat Tire Patch Repair</li>
					</ul>
				</dd>
			</dl>
			<dl class="col fl">
				<dt> <img src="public/images/content/sec3img2.jpg" alt="Service Image 2"> </dt>
				<dd>
					<ul>
						<li> <span>•</span> Tire Balancing </li>
						<li> <span>•</span> Tire Mounting </li>
						<li> <span>•</span> Tire Pressure Monitoring </li>
						<li> <span>•</span> Sensor Service </li>
						<li> <span>•</span> Valve Stem Replacement </li>
					</ul>
				</dd>
			</dl>
			<dl class="col fl">
				<dt> <img src="public/images/content/sec3img3.jpg" alt="Service Image 3"> </dt>
				<dd>
					<ul>
						<li> <span>•</span> Wheel Alignment & Wheel Balance </li>
						<li> <span>•</span> Steering Repair and Suspension Repair </li>
						<li> <span>•</span> Brake Repair Service </li>
						<li> <span>•</span> Computer Diagnostic </li>
						<li> <span>•</span> Fuel System Repair </li>
					</ul>
				</dd>
			</dl>
			<div class="clearfix"></div>
		</div>
		<div class="sec3Bot">
			<div class="sec3BotLeft col fl">
				<h3>Additional Services</h3>
				<ul>
					<li> <span>•</span> Decals & Graphic Designs</li>
					<li> <span>•</span> Electronic Taxi Meters</li>
					<li> <span>•</span> Installation & Calibration</li>
					<li> <span>•</span> Centrodyne</li>
					<li> <span>•</span> Pulsar Taxi Meters</li>
					<li> <span>•</span> Transportation Phone Repair</li>
					<li> <span>•</span> Verifone System For Transportation</li>
					<li> <span>•</span> Credit Card Systems</li>
				</ul>
			</div>
			<div class="sec3BotRight col fl">
				<h3>UBER DRIVERS - LYFT DRIVERS AND TAXI DRIVERS</h3>
				<p>are welcome with special rates for oil change full </p>
				<table>
					<tr>
						<td>SYNTHETIC</td>
						<td>$45.00</td>
					</tr>
					<tr>
						<td>REPAIRS RATE </td>
						<td>$70.00</td>
					</tr>
					<tr>
						<td></td>
						<td>AN HOUR (FOR TRANSPORTATION)</td>
					</tr>
				</table>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<div id="section4">
	<div class="row">
		<h2>OUR REVIEWS</h2>
		<h3>What they say about us...</h3>
		<p>I highly recommend this place. Sal the owner was very nice and helpful. When I brought my car in for the first time, they gave a great price (lower than other shops) and fixed my car beyond my expectations.!</p>
		<a href="review#content" class="btn">READ MORE</a>
	</div>
</div>
<div id="section5">
	<div class="row">
		<h2>OUR GALLERY</h2>
		<h3>What makes us the best!</h3>
		<div class="container">
			<img src="public/images/content/gallery1.jpg" alt="Gallery Image 1">
			<img src="public/images/content/gallery2.jpg" alt="Gallery Image 2">
			<img src="public/images/content/gallery3.jpg" alt="Gallery Image 3">
			<img src="public/images/content/gallery4.jpg" alt="Gallery Image 4">
		</div>
		<p>If you’re not sure what’s wrong with your car, schedule an appointment today. Our trusted mechanics take care of everything from an oil change and new tires to a complete car inspection that will provide you with a better understanding of the auto repairs you may need.</p>
		<a href="gallery#content" class="btn2">READ MORE</a>
	</div>
</div>
<div id="section6">
	<div class="row">
		<img src="public/images/content/logo1.jpg" alt="Logo Image 1">
		<img src="public/images/content/logo2.jpg" alt="Logo Image 2">
		<img src="public/images/content/logo3.jpg" alt="Logo Image 3">
		<img src="public/images/content/logo4.jpg" alt="Logo Image 4">
		<img src="public/images/content/logo5.jpg" alt="Logo Image 5">
	</div>
</div>
<div id="section7">
	<div class="row">
		<h2>CONTACT US</h2>
		<h3>Send us a message</h3>
		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<div class="container-1">
				<input type="text" name="name" placeholder="Name:">
				<input type="text" name="phone" placeholder="Phone:">
				<input type="text" name="email" placeholder="Email:">
			</div>
			<div class="container-2">
				<textarea name="message" cols="30" rows="10" placeholder="Message/Questions:"></textarea>
			</div>
			<div class="container-3">
				<div class="col-1 fl">
					<div class="row-1">
						<input type="checkbox" name="consent" class="consentBox">
						<p class="consent">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.</p>
					</div>
					<div class="row-2">
						<?php if( $this->siteInfo['policy_link'] ): ?>
						<input type="checkbox" name="termsConditions" class="termsBox"/>
						<p class="consent">I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a></ class="consent"p>
						<?php endif ?>
					</div>
				</div>
				<div class="col-2 fr">
					<div class="g-recaptcha"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<button type="submit" class="ctcBtn btn" disabled>SUBMIT FORM</button>
		</form>
	</div>
</div>
