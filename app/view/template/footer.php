<footer>
	<div id="footer">
		<div class="row">
			<div class="footTop">
				<dl>
					<dt class="fa fa-phone"><span>CALL US</span></dt>
					<dd>
						<?php $this->info(["phone","tel"]); ?><br>
						<?php $this->info(["phone2","tel"]);?>
					</dd>
				</dl>
				<dl>
					<dt class="fa fa-fax"><span>FAX</span></dt>
					<dd>
						<?php $this->info(["fax","tel"]); ?>
					</dd>
				</dl>
				<dl>
					<dt class="fa fa-envelope-o"><span>E-MAIL US</span></dt>
					<dd>
						<?php $this->info(["email","mailto"]); ?><br>
						<?php $this->info(["email2","mailto"]); ?>
					</dd>
				</dl>
				<dl>
					<dt class="fa fa-share-alt"><span>FOLLOW US</span></dt>
					<dd>
						<p>
							<a href="<?php $this->info("fb_link"); ?>" class="socialico" target="_blank">F</a>
							<a href="<?php $this->info("tt_link"); ?>" class="socialico" target="_blank">L</a>
							<a href="<?php $this->info("yt_link"); ?>" class="socialico" target="_blank">X</a>
							<a href="<?php $this->info("rss_link"); ?>" class="socialico" target="_blank">R</a>
						</p>
					</dd>
				</dl>
			</div>
			<a href="tel:517-259-4632" class="btn">CALL US NOW!</a>
		</div>
		<p class="copy">
			Copyright © <?php echo date("Y"); ?>. <?php $this->info("company_name"); ?> All Rights Reserved. Tearms Of Use.
			<?php if( $this->siteInfo['policy_link'] ): ?>
				<a href="<?php $this->info("policy_link"); ?>">Privacy Policy</a>.
			<?php endif ?>
		</p>
		<p class="silver"><img src="public/images/scnt.png" alt="" class="company-logo" /><a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank">Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>
	</div>
</footer>
<textarea id="g-recaptcha-response" class="destroy-on-load"></textarea>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo URL; ?>public/scripts/sendform.js" data-view="<?php echo $view; ?>" id="sendform"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>  -->
<script src="<?php echo URL; ?>public/scripts/responsive-menu.js"></script>
<script src="https://unpkg.com/sweetalert2@7.20.10/dist/sweetalert2.all.js"></script>

<?php if( $this->siteInfo['cookie'] ): ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	<script src="<?php echo URL; ?>public/scripts/cookie-script.js"></script>
<?php endif ?>

<?php if(in_array($view,["home","contact"])): ?>
	<script src='//www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit' async defer></script>
	<script>
		var captchaCallBack = function() {
			$('.g-recaptcha').each(function(index, el) {
				var recaptcha = grecaptcha.render(el, {'sitekey' : '<?php $this->info("site_key");?>'});
				$( '.destroy-on-load' ).remove();
			})
		};

		$('.consentBox').click(function () {
		    if ($(this).is(':checked')) {
		    	if($('.termsBox').length){
		    		if($('.termsBox').is(':checked')){
		        		$('.ctcBtn').removeAttr('disabled');
		        	}
		    	}else{
		        	$('.ctcBtn').removeAttr('disabled');
		    	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

		$('.termsBox').click(function () {
		    if ($(this).is(':checked')) {
	    		if($('.consentBox').is(':checked')){
	        		$('.ctcBtn').removeAttr('disabled');
	        	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

	</script>

<?php endif; ?>


<?php if ($view == "gallery"): ?>
	<script type="text/javascript" src="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo URL; ?>public/scripts/jquery.pajinate.js"></script>
	<script>
		$('#gall1').pajinate({ num_page_links_to_display : 3, items_per_page : 10 });
		$('.fancy').fancybox({
			helpers: {
				title : {
					type : 'over'
				}
			}
		});
	</script>
<?php endif; ?>

<a class="cta" href="tel:<?php $this->info("phone") ;?>"><span class="ctc-hide">Call To Action Button</span></a>

<?php $this->checkSuspensionFooter(); ?>
</body>
</html>
